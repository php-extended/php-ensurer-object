<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ensurer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ensurer\EnsurerIterator;
use PhpExtended\Ensurer\LooseEnsurer;
use PHPUnit\Framework\TestCase;

/**
 * LooseEnsurerTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ensurer\BaseEnsurer
 * @covers \PhpExtended\Ensurer\LooseEnsurer
 *
 * @internal
 *
 * @small
 */
class LooseEnsurerTest extends TestCase
{
	
	/**
	 * The ensurer to test.
	 * 
	 * @var LooseEnsurer
	 */
	protected LooseEnsurer $_ensurer;
	
	public function testToString() : void
	{
		$object = $this->_ensurer;
		$this->assertEquals(\get_class($object).'@'.\spl_object_hash($object), $object->__toString());
	}
	
	public function testBooleanNull() : void
	{
		$this->assertFalse($this->_ensurer->asBoolean(null));
	}
	
	public function testBooleanNull2() : void
	{
		$this->assertNull($this->_ensurer->asBooleanOrNull('\\n'));
	}
	
	public function testBooleanTrue() : void
	{
		$this->assertTrue($this->_ensurer->asBoolean(true));
	}
	
	public function testBooleanFalse() : void
	{
		$this->assertFalse($this->_ensurer->asBoolean(false));
	}
	
	public function testBooleanZero() : void
	{
		$this->assertFalse($this->_ensurer->asBoolean(0));
	}
	
	public function testBooleanOne() : void
	{
		$this->assertTrue($this->_ensurer->asBoolean(1));
	}
	
	public function testBooleanEmptyString() : void
	{
		$this->assertFalse($this->_ensurer->asBoolean(''));
	}
	
	public function testBooleanTrueString() : void
	{
		$this->assertTrue($this->_ensurer->asBoolean('true'));
	}
	
	public function testBooleanFalseString() : void
	{
		$this->assertFalse($this->_ensurer->asBoolean('false'));
	}
	
	public function testBooleanNonEmptyString() : void
	{
		$this->assertTrue($this->_ensurer->asBoolean('a'));
	}
	
	public function testBooleanEmptyArray() : void
	{
		$this->assertFalse($this->_ensurer->asBoolean([]));
	}
	
	public function testBooleanNonEmptyArray() : void
	{
		$this->assertTrue($this->_ensurer->asBoolean([null]));
	}
	
	public function testBooleanObject() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asBoolean(new stdClass());
	}
	
	public function testArrayOfBooleans() : void
	{
		$this->assertEquals([true], $this->_ensurer->asArrayOfBooleans([true]));
	}
	
	
	
	public function testIntegerNull() : void
	{
		$this->assertEquals(0, $this->_ensurer->asInteger(null));
	}
	
	public function testIntegerTrue() : void
	{
		$this->assertEquals(1, $this->_ensurer->asInteger(true));
	}
	
	public function testIntegerFalse() : void
	{
		$this->assertEquals(0, $this->_ensurer->asInteger(false));
	}
	
	public function testIntegerZero() : void
	{
		$this->assertEquals(0, $this->_ensurer->asInteger(0));
	}
	
	public function testIntegerOne() : void
	{
		$this->assertEquals(1, $this->_ensurer->asInteger(1));
	}
	
	public function testIntegerEmptyString() : void
	{
		$this->assertEquals(0, $this->_ensurer->asInteger(''));
	}
	
	public function testIntegerNonEmptyString() : void
	{
		$this->assertEquals(1, $this->_ensurer->asInteger('1'));
	}
	
	public function testIntegerEmptyArray() : void
	{
		$this->assertEquals(0, $this->_ensurer->asInteger([]));
	}
	
	public function testIntegerNonEmptyArray() : void
	{
		$this->assertEquals(1, $this->_ensurer->asInteger([null]));
	}
	
	public function testIntegerObject() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asInteger(new stdClass());
	}
	
	public function testArrayOfIntegers() : void
	{
		$this->assertEquals([1], $this->_ensurer->asArrayOfIntegers([1]));
	}
	
	
	
	public function testFloatNull() : void
	{
		$this->assertEquals(0.0, $this->_ensurer->asFloat(null));
	}
	
	public function testFloatTrue() : void
	{
		$this->assertEquals(1.0, $this->_ensurer->asFloat(true));
	}
	
	public function testFloatFalse() : void
	{
		$this->assertEquals(0.0, $this->_ensurer->asFloat(false));
	}
	
	public function testFloatZero() : void
	{
		$this->assertEquals(0.0, $this->_ensurer->asFloat(0));
	}
	
	public function testFloatOne() : void
	{
		$this->assertEquals(1.0, $this->_ensurer->asFloat(1));
	}
	
	public function testFloatEmptyString() : void
	{
		$this->assertEquals(0.0, $this->_ensurer->asFloat(''));
	}
	
	public function testFloatNonEmptyString() : void
	{
		$this->assertEquals(1.0, $this->_ensurer->asFloat('1'));
	}
	
	public function testFloatEmptyArray() : void
	{
		$this->assertEquals(0.0, $this->_ensurer->asFloat([]));
	}
	
	public function testFloatNonEmptyArray() : void
	{
		$this->assertEquals(1.0, $this->_ensurer->asFloat([null]));
	}
	
	public function testFloatObject() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asFloat(new stdClass());
	}
	
	public function testArrayOfFloats() : void
	{
		$this->assertEquals([1.0], $this->_ensurer->asArrayOfFloats([1.0]));
	}
	
	
	
	public function testStringNull() : void
	{
		$this->assertEquals('', $this->_ensurer->asString(null));
	}
	
	public function testStringTrue() : void
	{
		$this->assertEquals('1', $this->_ensurer->asString(true));
	}
	
	public function testStringFalse() : void
	{
		$this->assertEquals('', $this->_ensurer->asString(false));
	}
	
	public function testStringZero() : void
	{
		$this->assertEquals('0', $this->_ensurer->asString(0));
	}
	
	public function testStringOne() : void
	{
		$this->assertEquals('1', $this->_ensurer->asString(1));
	}
	
	public function testStringEmptyString() : void
	{
		$this->assertEquals('', $this->_ensurer->asString(''));
	}
	
	public function testStringNonEmptyString() : void
	{
		$this->assertEquals('a', $this->_ensurer->asString('a'));
	}
	
	public function testStringEmptyArray() : void
	{
		$this->assertEquals('[]', $this->_ensurer->asString([]));
	}
	
	public function testStringNonEmptyArray() : void
	{
		$this->assertEquals('', $this->_ensurer->asString([null]));
	}
	
	public function testStringNonEmptyArray2() : void
	{
		$this->assertEquals('[1, 2]', $this->_ensurer->asString([1, 2]));
	}
	
	public function testStringObject() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asString(new stdClass());
	}
	
	public function testStringObjectStringable() : void
	{
		$this->assertEquals('hello', $this->_ensurer->asString(new class()
		{
			public function __toString() : string
			{
				return 'hello';
			}
		}));
	}
	
	public function testArrayOfStrings() : void
	{
		$this->assertEquals(['hello'], $this->_ensurer->asArrayOfStrings(['hello']));
	}
	
	
	
	public function testDateTimeNull() : void
	{
		$this->assertEquals(new DateTimeImmutable('@0'), $this->_ensurer->asDateTime(null, []));
	}
	
	public function testDateTimeFormatFound() : void
	{
		$this->assertEquals('2000-01-01', $this->_ensurer->asDateTime('2000-01-01', ['Y-m-d'])->format('Y-m-d'));
	}
	
	public function testDateTimeFormatNotFound() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asDateTime('toto', ['Y-m-d']);
	}
	
	public function testDateTimeObject() : void
	{
		$expected = new DateTimeImmutable();
		$this->assertEquals($expected, $this->_ensurer->asDateTime($expected));
	}
	
	public function testArrayOfDateTimes() : void
	{
		$expected = new DateTimeImmutable();
		$this->assertEquals([$expected], $this->_ensurer->asArrayOfDateTimes([$expected]));
	}
	
	
	
	public function testArrayNull() : void
	{
		$this->assertEquals([], $this->_ensurer->asArray(null));
	}
	
	public function testArrayScalar() : void
	{
		$this->assertEquals(['foobar'], $this->_ensurer->asArray('foobar'));
	}
	
	public function testArrayArray() : void
	{
		$this->assertEquals([1, 2], $this->_ensurer->asArray([1, 2]));
	}
	
	public function testArrayObject1() : void
	{
		$this->assertEquals([1, 2], $this->_ensurer->asArray(new class()
		{
			public function __toArray() : array
			{
				return [1, 2];
			}
		}));
	}
	
	public function testArrayObject2() : void
	{
		$this->assertEquals([2, 3], $this->_ensurer->asArray(new class()
		{
			public function toArray() : array
			{
				return [2, 3];
			}
		}));
	}
	
	public function testArrayTraversable() : void
	{
		$this->assertEquals([3, 4], $this->_ensurer->asArray(new class() implements IteratorAggregate
		{
			public $prop1 = 3;

			public $prop2 = 4;
			
			public function getIterator() : Iterator
			{
				return new ArrayIterator([$this->prop1, $this->prop2]);
			}
		}));
	}
	
	public function testArrayObject() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asArray(new stdClass());
	}
	
	public function testObjectOfNull() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asObjectOf(null, stdClass::class);
	}
	
	public function testObjectOfNull2() : void
	{
		$this->assertNull($this->_ensurer->asObjectOfOrNull(null, stdClass::class));
	}
	
	public function testObjectOfClass() : void
	{
		$this->assertEquals(new stdClass(), $this->_ensurer->asObjectOfOrNull(new stdClass(), stdClass::class));
	}
	
	public function testArrayOfEmpty() : void
	{
		$this->assertEquals([], $this->_ensurer->asArrayOf([], stdClass::class));
	}
	
	public function testArrayOfNonEmpty() : void
	{
		$this->assertEquals([new stdClass()], $this->_ensurer->asArrayOf([new stdClass()], stdClass::class));
	}
	
	public function testIteratorOfNull() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asIteratorOf(null, stdClass::class);
	}
	
	public function testIteratorOfArray() : void
	{
		$this->assertEquals(
			new EnsurerIterator($this->_ensurer, new ArrayIterator([new stdClass()]), stdClass::class),
			$this->_ensurer->asIteratorOf([new stdClass()], stdClass::class),
		);
	}
	
	public function testIteratorOfIterator() : void
	{
		$this->assertEquals(
			new EnsurerIterator($this->_ensurer, new ArrayIterator([new stdClass()]), stdClass::class),
			$this->_ensurer->asIteratorOf(new ArrayIterator([new stdClass()]), stdClass::class),
		);
	}
	
	public function testIteratorOfObject() : void
	{
		$this->assertEquals(
			new EnsurerIterator($this->_ensurer, new ArrayIterator([new stdClass()]), stdClass::class),
			$this->_ensurer->asIteratorOf(new stdClass(), stdClass::class),
		);
	}
	
	public function testAsListOfBooleans() : void
	{
		$this->assertEquals([true], $this->_ensurer->asListOfBooleans([true]));
	}
	
	public function testAsMapOfBooleans() : void
	{
		$this->assertEquals(['0' => true], $this->_ensurer->asMapOfBooleans([true]));
	}
	
	public function testAsListOfIntegers() : void
	{
		$this->assertEquals([1], $this->_ensurer->asListOfIntegers([1]));
	}
	
	public function testAsMapOfIntegers() : void
	{
		$this->assertEquals(['0' => 1], $this->_ensurer->asMapOfIntegers([1]));
	}
	
	public function testAsListOfFloats() : void
	{
		$this->assertEquals([1.5], $this->_ensurer->asListOfFloats([1.5]));
	}
	
	public function testAsMapOfFloats() : void
	{
		$this->assertEquals(['0' => 1.5], $this->_ensurer->asMapOfFloats([1.5]));
	}
	
	public function testAsListOfStrings() : void
	{
		$this->assertEquals(['str'], $this->_ensurer->asListOfStrings(['str']));
	}
	
	public function testAsMapOfStrings() : void
	{
		$this->assertEquals(['0' => 'str'], $this->_ensurer->asMapOfStrings(['str']));
	}
	
	public function testAsListOfDateTimes() : void
	{
		$dt = new DateTimeImmutable();
		$this->assertEquals([$dt], $this->_ensurer->asListOfDateTimes([$dt]));
	}
	
	public function testAsMapOfDateTimes() : void
	{
		$dt = new DateTimeImmutable();
		$this->assertEquals(['0' => $dt], $this->_ensurer->asMapOfDateTimes([$dt]));
	}
	
	public function testAsList() : void
	{
		$expected = [true, 1, 1.5, 'str', new DateTimeImmutable()];
		$this->assertEquals($expected, $this->_ensurer->asList($expected));
	}
	
	public function testAsMap() : void
	{
		$dt = new DateTimeImmutable();
		$given = [true, 1, 1.5, 'str', $dt];
		$expected = ['0' => true, '1' => 1, '2' => 1.5, '3' => 'str', '4' => $dt];
		$this->assertEquals($expected, $this->_ensurer->asMap($given));
	}
	
	public function testAsListOf() : void
	{
		$expected = [new stdClass()];
		$this->assertEquals($expected, $this->_ensurer->asListOf($expected, stdClass::class));
	}
	
	public function testAsMapOf() : void
	{
		$this->assertEquals(['0' => new stdClass()], $this->_ensurer->asMapOf([new stdClass()], stdClass::class));
	}
	
	public function testAsIntIteratorOf() : void
	{
		foreach($this->_ensurer->asIntIteratorOf([new stdClass()], stdClass::class) as $key => $value)
		{
			$this->assertIsInt($key);
			$this->assertInstanceOf(stdClass::class, $value);
		}
	}
	
	public function testAsStringIteratorOf() : void
	{
		foreach($this->_ensurer->asStringIteratorOf([new stdClass()], stdClass::class) as $key => $value)
		{
			$this->assertIsString($key);
			$this->assertInstanceOf(stdClass::class, $value);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_ensurer = new LooseEnsurer();
	}
	
}
