<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ensurer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ensurer\IntegerKeyIterator;
use PhpExtended\Ensurer\LooseEnsurer;
use PHPUnit\Framework\TestCase;

/**
 * IntegerKeyIteratorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ensurer\IntegerKeyIterator
 *
 * @internal
 *
 * @small
 */
class IntegerKeyIteratorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var IntegerKeyIterator
	 */
	protected IntegerKeyIterator $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testContents() : void
	{
		foreach($this->_object as $key => $value)
		{
			$this->assertIsInt($key);
			$this->assertEquals(new stdClass(), $value);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new IntegerKeyIterator(new LooseEnsurer(), new ArrayIterator([new stdClass()]));
	}
	
}
