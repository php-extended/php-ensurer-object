<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ensurer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ensurer\EnsurerIterator;
use PhpExtended\Ensurer\StrictEnsurer;
use PHPUnit\Framework\TestCase;

/**
 * StrictEnsurerTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ensurer\BaseEnsurer
 * @covers \PhpExtended\Ensurer\StrictEnsurer
 *
 * @internal
 *
 * @small
 */
class StrictEnsurerTest extends TestCase
{
	
	/**
	 * The ensurer to test.
	 *
	 * @var StrictEnsurer
	 */
	protected StrictEnsurer $_ensurer;	
	
	public function testToString() : void
	{
		$object = $this->_ensurer;
		$this->assertEquals(\get_class($object).'@'.\spl_object_hash($object), $object->__toString());
	}
	
	public function testBooleanONNull() : void
	{
		$this->assertNull($this->_ensurer->asBooleanOrNull(null));
	}
	
	public function testBooleanONTrue() : void
	{
		$this->assertTrue($this->_ensurer->asBooleanOrNull(true));
	}
	
	public function testBooleanONFalse() : void
	{
		$this->assertFalse($this->_ensurer->asBooleanOrNull(false));
	}
	
	public function testBooleanONZero() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asBooleanOrNull(0);
	}
	
	public function testBooleanONOne() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asBooleanOrNull(1);
	}
	
	public function testBooleanONEmptyString() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asBooleanOrNull('');
	}
	
	public function testBooleanONTrueString() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asBooleanOrNull('true');
	}
	
	public function testBooleanONFalseString() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asBooleanOrNull('false');
	}
	
	public function testBooleanONNonEmptyString() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asBooleanOrNull('a');
	}
	
	public function testBooleanONEmptyArray() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asBooleanOrNull([]);
	}
	
	public function testBooleanONNonEmptyArray() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asBooleanOrNull([null]);
	}
	
	public function testBooleanONObject() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asBooleanOrNull(new stdClass());
	}
	
	
	
	public function testBooleanNull() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asBoolean(null);
	}
	
	public function testBooleanTrue() : void
	{
		$this->assertTrue($this->_ensurer->asBoolean(true));
	}
	
	public function testBooleanFalse() : void
	{
		$this->assertFalse($this->_ensurer->asBoolean(false));
	}
	
	public function testBooleanZero() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asBoolean(0);
	}
	
	public function testBooleanOne() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->assertTrue($this->_ensurer->asBoolean(1));
	}
	
	public function testBooleanEmptyString() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asBoolean('');
	}
	
	public function testBooleanTrueString() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asBoolean('true');
	}
	
	public function testBooleanFalseString() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asBoolean('false');
	}
	
	public function testBooleanNonEmptyString() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asBoolean('a');
	}
	
	public function testBooleanEmptyArray() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asBoolean([]);
	}
	
	public function testBooleanNonEmptyArray() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asBoolean([null]);
	}
	
	public function testBooleanObject() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asBoolean(new stdClass());
	}
	
	public function testArrayOfBooleans() : void
	{
		$this->assertEquals([true], $this->_ensurer->asArrayOfBooleans([true]));
	}
	
	
	
	public function testIntegerONNull() : void
	{
		$this->assertNull($this->_ensurer->asIntegerOrNull(null));
	}
	
	public function testIntegerONTrue() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asIntegerOrNull(true);
	}
	
	public function testIntegerONFalse() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asIntegerOrNull(false);
	}
	
	public function testIntegerONZero() : void
	{
		$this->assertEquals(0, $this->_ensurer->asIntegerOrNull(0));
	}
	
	public function testIntegerONOne() : void
	{
		$this->assertEquals(1, $this->_ensurer->asIntegerOrNull(1));
	}
	
	public function testIntegerONEmptyString() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asIntegerOrNull('');
	}
	
	public function testIntegerONNonEmptyString() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asIntegerOrNull('1');
	}
	
	public function testIntegerONEmptyArray() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asIntegerOrNull([]);
	}
	
	public function testIntegerONNonEmptyArray() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asIntegerOrNull([null]);
	}
	
	public function testIntegerONObject() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asIntegerOrNull(new stdClass());
	}
	
	public function testIntegerNull() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asInteger(null);
	}
	
	public function testIntegerTrue() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asInteger(true);
	}
	
	public function testIntegerFalse() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asInteger(false);
	}
	
	public function testIntegerZero() : void
	{
		$this->assertEquals(0, $this->_ensurer->asInteger(0));
	}
	
	public function testIntegerOne() : void
	{
		$this->assertEquals(1, $this->_ensurer->asInteger(1));
	}
	
	public function testIntegerEmptyString() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asInteger('');
	}
	
	public function testIntegerNonEmptyString() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asInteger('1');
	}
	
	public function testIntegerEmptyArray() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asInteger([]);
	}
	
	public function testIntegerNonEmptyArray() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asInteger([null]);
	}
	
	public function testIntegerObject() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asInteger(new stdClass());
	}
	
	public function testArrayOfIntegers() : void
	{
		$this->assertEquals([1], $this->_ensurer->asArrayOfIntegers([1]));
	}
	
	
	
	public function testFloatONNull() : void
	{
		$this->assertNull($this->_ensurer->asFloatOrNull(null));
	}
	
	public function testFloatONTrue() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asFloatOrNull(true);
	}
	
	public function testFloatONFalse() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asFloatOrNull(false);
	}
	
	public function testFloatONZero() : void
	{
		$this->assertEquals(0.0, $this->_ensurer->asFloatOrNull(0));
	}
	
	public function testFloatONOne() : void
	{
		$this->assertEquals(1.0, $this->_ensurer->asFloatOrNull(1));
	}
	
	public function testFloatONEmptyString() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asFloatOrNull('');
	}
	
	public function testFloatONNonEmptyString() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asFloatOrNull('1');
	}
	
	public function testFloatONEmptyArray() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asFloatOrNull([]);
	}
	
	public function testFloatONNonEmptyArray() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asFloatOrNull([null]);
	}
	
	public function testFloatONObject() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asFloatOrNull(new stdClass());
	}
	
	public function testFloatNull() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asFloat(null);
	}
	
	public function testFloatTrue() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asFloat(true);
	}
	
	public function testFloatFalse() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asFloat(false);
	}
	
	public function testFloatZero() : void
	{
		$this->assertEquals(0.0, $this->_ensurer->asFloat(0));
	}
	
	public function testFloatOne() : void
	{
		$this->assertEquals(1.0, $this->_ensurer->asFloat(1));
	}
	
	public function testFloatEmptyString() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asFloat('');
	}
	
	public function testFloatNonEmptyString() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asFloat('1');
	}
	
	public function testFloatEmptyArray() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asFloat([]);
	}
	
	public function testFloatNonEmptyArray() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asFloat([null]);
	}
	
	public function testFloatObject() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asFloat(new stdClass());
	}
	
	public function testArrayOfFloats() : void
	{
		$this->assertEquals([1.0], $this->_ensurer->asArrayOfFloats([1.0]));
	}
	
	
	
	public function testStringONNull() : void
	{
		$this->assertNull($this->_ensurer->asStringOrNull(null));
	}
	
	public function testStringONTrue() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asStringOrNull(true);
	}
	
	public function testStringONFalse() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asStringOrNull(false);
	}
	
	public function testStringONZero() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asStringOrNull(0);
	}
	
	public function testStringONOne() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asStringOrNull(1);
	}
	
	public function testStringONEmptyString() : void
	{
		$this->assertEquals('', $this->_ensurer->asString(''));
	}
	
	public function testStringONNonEmptyString() : void
	{
		$this->assertEquals('a', $this->_ensurer->asStringOrNull('a'));
	}
	
	public function testStringONEmptyArray() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asStringOrNull([]);
	}
	
	public function testStringONNonEmptyArray() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asStringOrNull([null]);
	}
	
	public function testStringONNonEmptyArray2() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asStringOrNull([1, 2]);
	}
	
	public function testStringONObject() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asStringOrNull(new stdClass());
	}
	
	public function testStringONObjectStringable() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asStringOrNull(new class()
		{
			public function __toString() : string
			{
				return 'hello';
			}
		});
	}
	
	public function testStringNull() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asString(null);
	}
	
	public function testStringTrue() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asString(true);
	}
	
	public function testStringFalse() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asString(false);
	}
	
	public function testStringZero() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asString(0);
	}
	
	public function testStringOne() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asString(1);
	}
	
	public function testStringEmptyString() : void
	{
		$this->assertEquals('', $this->_ensurer->asString(''));
	}
	
	public function testStringNonEmptyString() : void
	{
		$this->assertEquals('a', $this->_ensurer->asString('a'));
	}
	
	public function testStringEmptyArray() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asString([]);
	}
	
	public function testStringNonEmptyArray() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asString([null]);
	}
	
	public function testStringNonEmptyArray2() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asString([1, 2]);
	}
	
	public function testStringObject() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asString(new stdClass());
	}
	
	public function testStringObjectStringable() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asString(new class()
		{
			public function __toString() : string
			{
				return 'hello';
			}
		});
	}
	
	public function testArrayOfStrings() : void
	{
		$this->assertEquals(['hello'], $this->_ensurer->asArrayOfStrings(['hello']));
	}
	
	
	
	public function testDateTimeONNull() : void
	{
		$this->assertNull($this->_ensurer->asDateTimeOrNull(null, []));
	}
	
	public function testDateTimeONFormatFound() : void
	{
		$this->assertEquals('2000-01-01', $this->_ensurer->asDateTimeOrNull('2000-01-01', ['Y-m-d'])->format('Y-m-d'));
	}
	
	public function testDateTimeONFormatNotFound() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asDateTimeOrNull('toto', ['Y-m-d']);
	}
	
	public function testDateTimeONObject() : void
	{
		$expected = new DateTimeImmutable();
		$this->assertEquals($expected, $this->_ensurer->asDateTimeOrNull($expected));
	}
	
	public function testDateTimeNull() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asDateTime(null, []);
	}
	
	public function testDateTimeFormatFound() : void
	{
		$this->assertEquals('2000-01-01', $this->_ensurer->asDateTime('2000-01-01', ['Y-m-d'])->format('Y-m-d'));
	}
	
	public function testDateTimeFormatNotFound() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asDateTime('toto', ['Y-m-d']);
	}
	
	public function testDateTimeObject() : void
	{
		$expected = new DateTimeImmutable();
		$this->assertEquals($expected, $this->_ensurer->asDateTime($expected));
	}
	
	public function testArrayOfDateTimes() : void
	{
		$expected = new DateTimeImmutable();
		$this->assertEquals([$expected], $this->_ensurer->asArrayOfDateTimes([$expected]));
	}
	
	
	
	public function testArrayNull() : void
	{
		$this->assertEquals([], $this->_ensurer->asArray(null));
	}
	
	public function testArrayScalar() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->assertEquals(['foobar'], $this->_ensurer->asArray('foobar'));
	}
	
	public function testArrayArray() : void
	{
		$this->assertEquals([1, 2], $this->_ensurer->asArray([1, 2]));
	}
	
	public function testArrayObject1() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asArray(new class()
		{
			public function __toArray() : array
			{
				return [1, 2];
			}
		});
	}
	
	public function testArrayObject2() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asArray(new class()
		{
			public function toArray() : array
			{
				return [2, 3];
			}
		});
	}
	
	public function testArrayTraversable() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asArray(new class() implements IteratorAggregate
		{
			public $prop1 = 3;

			public $prop2 = 4;
			
			public function getIterator() : Iterator
			{
				return new ArrayIterator([$this->prop1, $this->prop2]);
			}
		});
	}
	
	public function testArrayObject() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asArray(new stdClass());
	}
	
	public function testObjectOfNull() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asObjectOf(null, stdClass::class);
	}
	
	public function testObjectOfNull2() : void
	{
		$this->assertNull($this->_ensurer->asObjectOfOrNull(null, stdClass::class));
	}
	
	public function testObjectOfClass() : void
	{
		$this->assertEquals(new stdClass(), $this->_ensurer->asObjectOfOrNull(new stdClass(), stdClass::class));
	}
	
	public function testArrayOfEmpty() : void
	{
		$this->assertEquals([], $this->_ensurer->asArrayOf([], stdClass::class));
	}
	
	public function testArrayOfNonEmpty() : void
	{
		$this->assertEquals([new stdClass()], $this->_ensurer->asArrayOf([new stdClass()], stdClass::class));
	}
	
	public function testIteratorOfNull() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_ensurer->asIteratorOf(null, stdClass::class);
	}
	
	public function testIteratorOfArray() : void
	{
		$this->assertEquals(
			new EnsurerIterator($this->_ensurer, new ArrayIterator([new stdClass()]), stdClass::class),
			$this->_ensurer->asIteratorOf([new stdClass()], stdClass::class),
		);
	}
	
	public function testIteratorOfIterator() : void
	{
		$this->assertEquals(
			new EnsurerIterator($this->_ensurer, new ArrayIterator([new stdClass()]), stdClass::class),
			$this->_ensurer->asIteratorOf(new ArrayIterator([new stdClass()]), stdClass::class),
		);
	}
	
	public function testIteratorOfObject() : void
	{
		$this->assertEquals(
			new EnsurerIterator($this->_ensurer, new ArrayIterator([new stdClass()]), stdClass::class),
			$this->_ensurer->asIteratorOf(new stdClass(), stdClass::class),
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_ensurer = new StrictEnsurer();
	}
	
}
