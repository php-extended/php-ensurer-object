<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ensurer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ensurer\BaseEnsurer;
use PHPUnit\Framework\TestCase;

/**
 * BaseEnsurerTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ensurer\BaseEnsurer
 *
 * @internal
 *
 * @small
 */
class BaseEnsurerTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var BaseEnsurer
	 */
	protected BaseEnsurer $_object;
	
	public function testToString() : void
	{
		$this->assertNotNull($this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = $this->createMock(BaseEnsurer::class);
	}
	
}
