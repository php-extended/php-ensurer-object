<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ensurer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ensurer\EnsurerIterator;
use PhpExtended\Ensurer\LooseEnsurer;
use PHPUnit\Framework\TestCase;

/**
 * EnsurerIteratorTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ensurer\EnsurerIterator
 *
 * @internal
 *
 * @small
 */
class EnsurerIteratorTest extends TestCase
{
	
	/**
	 * The iterator to test.
	 * 
	 * @var EnsurerIterator
	 */
	protected EnsurerIterator $_iterator;
	
	public function testToString() : void
	{
		$object = $this->_iterator;
		$this->assertEquals(\get_class($object).'@'.\spl_object_hash($object), $object->__toString());
	}
	
	public function testContents() : void
	{
		foreach($this->_iterator as $key => $value)
		{
			$this->assertIsInt($key);
			$this->assertEquals(new stdClass(), $value);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_iterator = new EnsurerIterator(new LooseEnsurer(), new ArrayIterator([new stdClass()]), stdClass::class);
	}
	
}
