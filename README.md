# php-extended/php-ensurer-object

An implementation of the php-ensure-interface library.

![coverage](https://gitlab.com/php-extended/php-ensurer-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-ensurer-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-ensurer-object ^8`


## Basic Usage

You may use this class the following way :

```php

use PhpExtended\Ensurer\NotNullLooseEnsurer;

$ensurer = new LooseEnsurer(); // alternatively new StrictEnsurer();

$ensurer->asBoolean(1); // return true
$ensurer->asString(null); // returns ""
$ensurer->asObjectOf(new \DateTime(), \DateTime::class); // returns the object
$ensurer->asObjectOf(new \DateTime(), Foo::class); // throws \InvalidArgumentException

```


## License

MIT (See [license file](LICENSE)).
