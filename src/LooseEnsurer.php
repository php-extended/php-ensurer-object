<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ensurer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ensurer;

use DateTimeImmutable;
use DateTimeInterface;
use InvalidArgumentException;
use Traversable;

/**
 * LooseEnsurer class file.
 * 
 * This class is a simple implementation of the EnsurerInterface that does all
 * of what is possible to transform the data into the wanted type.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 */
class LooseEnsurer extends BaseEnsurer
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asBooleanOrNull()
	 */
	public function asBooleanOrNull($value) : ?bool
	{
		if(null === $value)
		{
			return null;
		}
		
		if(\is_scalar($value))
		{
			if(\is_string($value))
			{
				if('' === $value)
				{
					return null;
				}
				
				$lowerval = \mb_strtolower($value);
				if(\in_array($lowerval, ['\\n', 'null'], true))
				{
					return null;
				}
				
				if(\in_array($lowerval, ['false', 'n', 'f', 'no'], true))
				{
					return false;
				}
				
				if(\in_array($lowerval, ['true', 'y', 't', 'yes'], true))
				{
					return true;
				}
			}
			
			return (bool) $value;
		}
		
		if(\is_array($value))
		{
			return 0 < \count($value);
		}
		
		$message = 'Impossible to transform value "{thing}" to boolean.';
		$context = ['{thing}' => $this->_inspector->inspect($value)];
		
		throw new InvalidArgumentException(\strtr($message, $context));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asBoolean()
	 */
	public function asBoolean($value) : bool
	{
		$value = $this->asBooleanOrNull($value);
		
		return $value ?? false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asIntegerOrNull()
	 */
	public function asIntegerOrNull($value) : ?int
	{
		if(null === $value)
		{
			return null;
		}
		
		if('' === $value)
		{
			return null;
		}
		
		if(\is_scalar($value))
		{
			return (int) $value;
		}
		
		if(\is_array($value))
		{
			return \count($value);
		}
		
		$message = 'Impossible to transform value "{thing}" to integer.';
		$context = ['{thing}' => $this->_inspector->inspect($value)];
		
		throw new InvalidArgumentException(\strtr($message, $context));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asInteger()
	 */
	public function asInteger($value) : int
	{
		$value = $this->asIntegerOrNull($value);
		
		return $value ?? 0;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asFloatOrNull()
	 */
	public function asFloatOrNull($value) : ?float
	{
		if(null === $value)
		{
			return null;
		}
		
		if('' === $value)
		{
			return null;
		}
		
		if(\is_scalar($value))
		{
			return (float) $value;
		}
		
		if(\is_array($value))
		{
			return (float) \count($value);
		}
		
		$message = 'Impossible to transform value "{thing}" to float.';
		$context = ['{thing}' => $this->_inspector->inspect($value)];
		
		throw new InvalidArgumentException(\strtr($message, $context));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asFloat()
	 */
	public function asFloat($value) : float
	{
		$value = $this->asFloatOrNull($value);
		
		return $value ?? 0.0;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asStringOrNull()
	 */
	public function asStringOrNull($value) : ?string
	{
		if(null === $value)
		{
			return null;
		}
		
		if('' === $value)
		{
			return null;
		}
		
		if(\is_scalar($value))
		{
			return (string) $value;
		}
		
		if(\is_array($value))
		{
			if(1 === \count($value))
			{
				$new = \reset($value);
				
				return $this->asString($new);
			}
			
			$parts = [];
			
			foreach($value as $part)
			{
				$parts[] = $this->asString($part);
			}
			
			return '['.\implode(', ', $parts).']';
		}
		
		/** @phpstan-ignore-next-line */
		if(\is_object($value))
		{
			if(\method_exists($value, '__toString'))
			{
				return (string) $value->__toString();
			}
		}
		
		$message = 'Impossible to transform value "{thing}" to string.';
		$context = ['{thing}' => $this->_inspector->inspect($value)];
		
		throw new InvalidArgumentException(\strtr($message, $context));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asString()
	 */
	public function asString($value) : string
	{
		$value = $this->asStringOrNull($value);
		
		return $value ?? '';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asDateTimeOrNull()
	 */
	public function asDateTimeOrNull($value, array $formats = []) : ?DateTimeInterface
	{
		if(null === $value)
		{
			return null;
		}
		
		if($value instanceof DateTimeInterface)
		{
			return $value;
		}
		
		foreach($formats as $format)
		{
			$dti = DateTimeImmutable::createFromFormat($format, $this->asString($value));
			if(false !== $dti)
			{
				return $dti;
			}
		}
		
		$message = 'Impossible to transform value "{thing}" to \\Datetime with given formats : "{list}".';
		$context = ['{thing}' => $this->_inspector->inspect($value), '{list}' => \implode('", "', $formats)];
		
		throw new InvalidArgumentException(\strtr($message, $context));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asDateTime()
	 */
	public function asDateTime($value, array $formats = []) : DateTimeInterface
	{
		$value = $this->asDateTimeOrNull($value, $formats);
		
		return $value ?? new DateTimeImmutable('@0');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asArray()
	 */
	public function asArray($value) : array
	{
		/** @psalm-suppress RiskyTruthyFalsyComparison */
		if(empty($value))
		{
			return [];
		}
		
		if(\is_scalar($value))
		{
			return [$value];
		}
		
		if(\is_array($value))
		{
			return $value;
		}
		
		if(\is_object($value))
		{
			/** @phpstan-ignore-next-line */
			if(\method_exists($value, '__toArray'))
			{
				/** @phpstan-ignore-next-line */ /** @psalm-suppress MixedMethodCall php >= 8.0 */
				return $this->asArray($value->__toArray());
			}
			
			/** @phpstan-ignore-next-line */
			if(\method_exists($value, 'toArray'))
			{
				/** @phpstan-ignore-next-line */ /** @psalm-suppress MixedMethodCall php >= 8.0 */
				return $this->asArray($value->toArray());
			}
			
			if($value instanceof Traversable)
			{
				$values = [];
				
				foreach($value as $v)
				{
					$values[] = $v;
				}
				
				return $values;
			}
		}
		
		$message = 'Impossible to transform value "{thing}" to array.';
		/** @psalm-suppress MixedArgumentTypeCoercion php >= 8.0 */
		$context = ['{thing}' => $this->_inspector->inspect($value)];
		
		throw new InvalidArgumentException(\strtr($message, $context));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asObjectOfOrNull()
	 */
	public function asObjectOfOrNull($value, string $className) : ?object
	{
		if(null === $value)
		{
			return null;
		}
		
		return $this->asObjectOf($value, $className);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asObjectOf()
	 * @psalm-suppress InvalidReturnType
	 */
	public function asObjectOf($value, string $className) : object
	{
		if($value instanceof $className)
		{
			/** @psalm-suppress InvalidReturnStatement */
			return $value;
		}
		
		$message = 'Impossible to transform value "{thing}" to "{class}".';
		$context = ['{thing}' => $this->_inspector->inspect($value), '{class}' => $className];
		
		throw new InvalidArgumentException(\strtr($message, $context));
	}
	
}
