<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ensurer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ensurer;

use DateTimeImmutable;
use DateTimeInterface;
use InvalidArgumentException;

/**
 * StrictEnsurer class file.
 *
 * This class is a simple implementation of the EnsurerInterface that only
 * accepts data that is already on the right type.
 *
 * @author Anastaszor
 */
class StrictEnsurer extends BaseEnsurer
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asBooleanOrNull()
	 */
	public function asBooleanOrNull($value) : ?bool
	{
		if(null === $value || true === $value || false === $value)
		{
			return $value;
		}
		
		$message = 'Impossible to transform value "{thing}" to boolean.';
		$context = ['{thing}' => $this->_inspector->inspect($value)];
		
		throw new InvalidArgumentException(\strtr($message, $context));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asBoolean()
	 */
	public function asBoolean($value) : bool
	{
		if(true === $value || false === $value)
		{
			return $value;
		}
		
		$message = 'Impossible to transform value "{thing}" to boolean.';
		$context = ['{thing}' => $this->_inspector->inspect($value)];
		
		throw new InvalidArgumentException(\strtr($message, $context));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asIntegerOrNull()
	 */
	public function asIntegerOrNull($value) : ?int
	{
		if(null === $value)
		{
			return null;
		}
		
		if(\is_int($value) || \is_float($value))
		{
			return (int) $value;
		}
		
		$message = 'Impossible to transform value "{thing}" to integer.';
		$context = ['{thing}' => $this->_inspector->inspect($value)];
		
		throw new InvalidArgumentException(\strtr($message, $context));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asInteger()
	 */
	public function asInteger($value) : int
	{
		if(\is_int($value) || \is_float($value))
		{
			return (int) $value;
		}
		
		$message = 'Impossible to transform value "{thing}" to integer.';
		$context = ['{thing}' => $this->_inspector->inspect($value)];
		
		throw new InvalidArgumentException(\strtr($message, $context));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asFloatOrNull()
	 */
	public function asFloatOrNull($value) : ?float
	{
		if(null === $value)
		{
			return null;
		}
		
		if(\is_float($value) || \is_int($value))
		{
			return (float) $value;
		}
		
		$message = 'Impossible to transform value "{thing}" to float.';
		$context = ['{thing}' => $this->_inspector->inspect($value)];
		
		throw new InvalidArgumentException(\strtr($message, $context));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asFloat()
	 */
	public function asFloat($value) : float
	{
		if(\is_float($value) || \is_int($value))
		{
			return (float) $value;
		}
		
		$message = 'Impossible to transform value "{thing}" to float.';
		$context = ['{thing}' => $this->_inspector->inspect($value)];
		
		throw new InvalidArgumentException(\strtr($message, $context));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asStringOrNull()
	 */
	public function asStringOrNull($value) : ?string
	{
		if(null === $value || \is_string($value))
		{
			return $value;
		}
		
		$message = 'Impossible to transform value "{thing}" to string.';
		$context = ['{thing}' => $this->_inspector->inspect($value)];
		
		throw new InvalidArgumentException(\strtr($message, $context));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asString()
	 */
	public function asString($value) : string
	{
		if(\is_string($value))
		{
			return $value;
		}
		
		$message = 'Impossible to transform value "{thing}" to string.';
		$context = ['{thing}' => $this->_inspector->inspect($value)];
		
		throw new InvalidArgumentException(\strtr($message, $context));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asDateTimeOrNull()
	 */
	public function asDateTimeOrNull($value, array $formats = []) : ?DateTimeInterface
	{
		if(null === $value)
		{
			return null;
		}
		
		if($value instanceof DateTimeInterface)
		{
			return $value;
		}
		
		foreach($formats as $format)
		{
			$dti = DateTimeImmutable::createFromFormat($format, $this->asString($value));
			if(false !== $dti)
			{
				return $dti;
			}
		}
		
		$message = 'Impossible to transform value "{thing}" to \\Datetime with given formats : "{list}".';
		$context = ['{thing}' => $this->_inspector->inspect($value), '{list}' => \implode('", "', $formats)];
		
		throw new InvalidArgumentException(\strtr($message, $context));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asDateTime()
	 */
	public function asDateTime($value, array $formats = []) : DateTimeInterface
	{
		if($value instanceof DateTimeInterface)
		{
			return $value;
		}
		
		foreach($formats as $format)
		{
			$dti = DateTimeImmutable::createFromFormat($format, $this->asString($value));
			if(false !== $dti)
			{
				return $dti;
			}
		}
		
		$message = 'Impossible to transform value "{thing}" to \\Datetime with given formats : "{list}".';
		$context = ['{thing}' => $this->_inspector->inspect($value), '{list}' => \implode('", "', $formats)];
		
		throw new InvalidArgumentException(\strtr($message, $context));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asArray()
	 */
	public function asArray($value) : array
	{
		/** @psalm-suppress RiskyTruthyFalsyComparison */
		if(empty($value))
		{
			return [];
		}
		
		if(\is_array($value))
		{
			return $value;
		}
		
		$message = 'Impossible to transform value "{thing}" to array.';
		/** @psalm-suppress MixedArgumentTypeCoercion php >= 8.0 */
		$context = ['{thing}' => $this->_inspector->inspect($value)];
		
		throw new InvalidArgumentException(\strtr($message, $context));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asObjectOfOrNull()
	 */
	public function asObjectOfOrNull($value, string $className) : ?object
	{
		if(null === $value)
		{
			return null;
		}
		
		return $this->asObjectOf($value, $className);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asObjectOf()
	 * @psalm-suppress InvalidReturnType
	 */
	public function asObjectOf($value, string $className) : object
	{
		if($value instanceof $className)
		{
			/** @psalm-suppress InvalidReturnStatement */
			return $value;
		}
		
		$message = 'Impossible to transform value "{thing}" to "{class}".';
		$context = ['{thing}' => $this->_inspector->inspect($value), '{class}' => $className];
		
		throw new InvalidArgumentException(\strtr($message, $context));
	}
	
}
