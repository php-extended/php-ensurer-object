<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ensurer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ensurer;

use InvalidArgumentException;
use Iterator;
use IteratorIterator;

/**
 * EnsurerIterator class file.
 * 
 * This class is an iterator that ensures the type of inner during streaming.
 * 
 * @author Anastaszor
 * @template T of object
 * @extends \IteratorIterator<integer|string, T, \Iterator<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>>
 * @psalm-suppress InvalidTemplateParam
 * @phpstan-ignore-next-line
 */
class EnsurerIterator extends IteratorIterator
{
	
	/**
	 * The ensurer.
	 * 
	 * @var EnsurerInterface
	 */
	protected EnsurerInterface $_ensurer;
	
	/**
	 * The expected class.
	 * 
	 * @var class-string<T>
	 */
	protected string $_className;
	
	/**
	 * Builds a new EnsurerIterator based on the given other iterator
	 * for the given class.
	 * 
	 * @param EnsurerInterface $ensurer
	 * @param Iterator<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $iterator
	 * @param class-string<T> $className
	 */
	public function __construct(EnsurerInterface $ensurer, Iterator $iterator, string $className)
	{
		parent::__construct($iterator);
		$this->_ensurer = $ensurer;
		$this->_className = $className;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::current()
	 * @throws InvalidArgumentException
	 */
	public function current() : object
	{
		return $this->_ensurer->asObjectOf(parent::current(), $this->_className);
	}
	
}
