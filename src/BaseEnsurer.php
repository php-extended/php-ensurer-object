<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ensurer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ensurer;

use ArrayIterator;
use InvalidArgumentException;
use Iterator;
use PhpExtended\Inspector\Inspector;
use PhpExtended\Inspector\InspectorInterface;

/**
 * BaseEnsurer class file.
 * 
 * This class encapsulates all the functions that all ensurers do.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.TooManyPublicMethods")
 */
abstract class BaseEnsurer implements EnsurerInterface
{
	
	/**
	 * The inner inspector.
	 *
	 * @var InspectorInterface
	 */
	protected InspectorInterface $_inspector;
	
	/**
	 * Builds a new Ensurer with the given inspector.
	 *
	 * @param ?InspectorInterface $inspector
	 */
	public function __construct(?InspectorInterface $inspector = null)
	{
		if(null === $inspector)
		{
			$inspector = new Inspector();
		}
		
		$this->_inspector = $inspector;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asArrayOfBooleans()
	 */
	public function asArrayOfBooleans($value) : array
	{
		$values = [];
		
		/** @psalm-suppress PossiblyInvalidArgument php >= 8.0 */
		foreach($this->asArray($value) as $key => $inner)
		{
			/** @psalm-suppress MixedArgumentTypeCoercion php >= 8.0 */
			$values[$key] = $this->asBoolean($inner);
		}
		
		return $values;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asListOfBooleans()
	 */
	public function asListOfBooleans($value) : array
	{
		return $this->asList($this->asArrayOfBooleans($value));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asMapOfBooleans()
	 */
	public function asMapOfBooleans($value) : array
	{
		return $this->asMap($this->asArrayOfBooleans($value));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asArrayOfIntegers()
	 */
	public function asArrayOfIntegers($value) : array
	{
		$values = [];
		
		/** @psalm-suppress PossiblyInvalidArgument php >= 8.0 */
		foreach($this->asArray($value) as $key => $inner)
		{
			/** @psalm-suppress MixedArgumentTypeCoercion php >= 8.0 */
			$values[$key] = $this->asInteger($inner);
		}
		
		return $values;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asListOfIntegers()
	 */
	public function asListOfIntegers($value) : array
	{
		return $this->asList($this->asArrayOfIntegers($value));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asMapOfIntegers()
	 */
	public function asMapOfIntegers($value) : array
	{
		return $this->asMap($this->asArrayOfIntegers($value));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asArrayOfFloats()
	 */
	public function asArrayOfFloats($value) : array
	{
		$values = [];
		
		/** @psalm-suppress PossiblyInvalidArgument php >= 8.0 */
		foreach($this->asArray($value) as $key => $inner)
		{
			/** @psalm-suppress MixedArgumentTypeCoercion php >= 8.0 */
			$values[$key] = $this->asFloat($inner);
		}
		
		return $values;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asListOfFloats()
	 */
	public function asListOfFloats($value) : array
	{
		return $this->asList($this->asArrayOfFloats($value));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asMapOfFloats()
	 */
	public function asMapOfFloats($value) : array
	{
		return $this->asMap($this->asArrayOfFloats($value));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asArrayOfStrings()
	 */
	public function asArrayOfStrings($value) : array
	{
		$values = [];
		
		/** @psalm-suppress PossiblyInvalidArgument php >= 8.0 */
		foreach($this->asArray($value) as $key => $value)
		{
			/** @psalm-suppress MixedArgumentTypeCoercion php >= 8.0 */
			$values[$key] = $this->asString($value);
		}
		
		return $values;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asListOfStrings()
	 */
	public function asListOfStrings($value) : array
	{
		return $this->asList($this->asArrayOfStrings($value));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asMapOfStrings()
	 */
	public function asMapOfStrings($value) : array
	{
		return $this->asMap($this->asArrayOfStrings($value));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asArrayOfDateTimes()
	 */
	public function asArrayOfDateTimes($value, array $formats = []) : array
	{
		$values = [];
		
		/** @psalm-suppress PossiblyInvalidArgument php >= 8.0 */
		foreach($this->asArray($value) as $key => $inner)
		{
			/** @psalm-suppress MixedArgumentTypeCoercion php >= 8.0 */
			$values[$key] = $this->asDateTime($inner, $formats);
		}
		
		return $values;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asListOfDateTimes()
	 */
	public function asListOfDateTimes($value, array $formats = []) : array
	{
		return $this->asList($this->asArrayOfDateTimes($value, $formats));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asMapOfDateTimes()
	 */
	public function asMapOfDateTimes($value, array $formats = []) : array
	{
		return $this->asMap($this->asArrayOfDateTimes($value, $formats));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asList()
	 */
	public function asList($value) : array
	{
		$res = [];
		
		foreach($this->asArray($value) as $key => $record)
		{
			$res[$this->asInteger($key)] = $record;
		}
		
		return $res;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asMap()
	 */
	public function asMap($value) : array
	{
		$res = [];
		
		foreach($this->asArray($value) as $key => $record)
		{
			$res[$this->asString($key)] = $record;
		}
		
		return $res;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asArrayOf()
	 */
	public function asArrayOf($value, string $className) : array
	{
		$res = [];
		
		/** @psalm-suppress PossiblyInvalidArgument php >= 8.0 */
		foreach($this->asArray($value) as $key => $object)
		{
			/** @psalm-suppress MixedArgumentTypeCoercion php >= 8.0 */
			$res[$key] = $this->asObjectOf($object, $className);
		}
		
		return $res;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asListOf()
	 */
	public function asListOf($value, string $className) : array
	{
		return $this->asList($this->asArrayOf($value, $className));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asMapOf()
	 */
	public function asMapOf($value, string $className) : array
	{
		return $this->asMap($this->asArrayOf($value, $className));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asIteratorOf()
	 */
	public function asIteratorOf($value, string $className) : Iterator
	{
		if(\is_array($value))
		{
			return new EnsurerIterator($this, new ArrayIterator($value), $className);
		}
		
		if(\is_object($value))
		{
			if($value instanceof Iterator)
			{
				/** @psalm-suppress MixedArgumentTypeCoercion php >= 8.0 */
				return new EnsurerIterator($this, $value, $className);
			}
			
			return new EnsurerIterator($this, new ArrayIterator([$value]), $className);
		}
		
		$message = 'Impossible to transform value "{thing}" to "{class}".';
		$context = ['{thing}' => $this->_inspector->inspect($value), '{class}' => Iterator::class];
		
		throw new InvalidArgumentException(\strtr($message, $context));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asIntIteratorOf()
	 */
	public function asIntIteratorOf($value, string $className) : Iterator
	{
		return new IntegerKeyIterator($this, $this->asIteratorOf($value, $className));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ensurer\EnsurerInterface::asStringIteratorOf()
	 */
	public function asStringIteratorOf($value, string $className) : Iterator
	{
		return new StringKeyIterator($this, $this->asIteratorOf($value, $className));
	}
	
}
