<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ensurer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ensurer;

use InvalidArgumentException;
use Iterator;
use IteratorIterator;

/**
 * StringKeyIterator class file.
 * 
 * This iterator iterates over objects and ensures that the keys are string
 * values.
 *
 * @author Anastaszor
 * @template T of object
 * @extends IteratorIterator<string, T, \Iterator<integer|string, T>>
 * @psalm-suppress InvalidTemplateParam
 * @phpstan-ignore-next-line
 */
class StringKeyIterator extends IteratorIterator
{
	
	/**
	 * The ensurer.
	 *
	 * @var EnsurerInterface
	 */
	protected EnsurerInterface $_ensurer;
	
	/**
	 * Builds a new EnsurerIterator based on the given other iterator
	 * for the given class.
	 *
	 * @param EnsurerInterface $ensurer
	 * @param Iterator<integer|string, T> $iterator
	 */
	public function __construct(EnsurerInterface $ensurer, Iterator $iterator)
	{
		parent::__construct($iterator);
		$this->_ensurer = $ensurer;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \IteratorIterator::key()
	 * @throws InvalidArgumentException
	 */
	public function key() : string
	{
		return $this->_ensurer->asString(parent::key());
	}
	
}
